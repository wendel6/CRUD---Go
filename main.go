package main

import (
	"encoding/json"
	"fmt"
	mux "github.com/gorilla/mux"
	"log"
	"net/http"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Person struct {
	gorm.Model

	Name  string
	Email string `gorm:"typevarchar(100);unique_index"`
	Books []Book
}

type Book struct {
	gorm.Model

	Title      string
	Author     string
	CallNumber int `gorm:"unique_index"`
	PersonID   int
}

var db *gorm.DB
var err error

func main() {
	// Loading environment variables
	host := os.Getenv("HOST")
	dbPort := os.Getenv("DBPORT")
	user := os.Getenv("USER")
	dbName := os.Getenv("NAME")
	password := os.Getenv("PASSWORD")

	// Database connection string
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=disable port=%s", host, user, dbName, password, dbPort)

	// Opening connection to database
	db, err = gorm.Open(postgres.Open(dbURI), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Successfully connected to database!")
	}

	// Make migrations to the database if they have not already been created
	db.AutoMigrate(&Person{})
	db.AutoMigrate(&Book{})

	//db.Create(person)
	//for idx := range books {
	//	db.Create(&books[idx])
	//}

	// API routes
	router := mux.NewRouter()
	router.HandleFunc("/people", getPeople).Methods("GET")
	router.HandleFunc("/books", getBooks).Methods("GET")

	router.HandleFunc("/person/{id}", getPerson).Methods("GET")
	router.HandleFunc("/book/{id}", getBook).Methods("GET")

	router.HandleFunc("/create/person", createPerson).Methods("POST")
	router.HandleFunc("/create/book", createBook).Methods("POST")

	router.HandleFunc("/delete/person/{id}", deletePerson).Methods("DELETE")
	router.HandleFunc("/delete/book/{id}", deleteBook).Methods("DELETE")

	http.ListenAndServe(":8080", router)
}

func getPeople(w http.ResponseWriter, r *http.Request) {
	var people []Person
	db.Find(&people)
	json.NewEncoder(w).Encode(&people)
}

func getBooks(w http.ResponseWriter, r *http.Request) {
	var books []Book
	db.Find(&books)
	json.NewEncoder(w).Encode(&books)
}

func getPerson(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)

	var person Person
	var books []Book

	db.First(&person, params["id"])
	db.Model(&person).Association("Books").Find(&books)

	person.Books = books

	json.NewEncoder(w).Encode(person)
}

func getBook(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)

	var book []Book

	db.First(&book, params["id"])

	json.NewEncoder(w).Encode(book)
}

func createPerson(w http.ResponseWriter, r *http.Request){
	var person Person
	json.NewDecoder(r.Body).Decode(&person)

	createdPerson := db.Create(&person)
	err = createdPerson.Error

	if err != nil {
		json.NewEncoder(w).Encode(err)
	} else {
		json.NewEncoder(w).Encode(&person)
	}
}

func createBook(w http.ResponseWriter, r *http.Request){
	var book Book
	json.NewDecoder(r.Body).Decode(&book)

	createdBook := db.Create(&book)
	err = createdBook.Error

	if err != nil {
		json.NewEncoder(w).Encode(err)
	} else {
		json.NewEncoder(w).Encode(&book)
	}
}

func deletePerson(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)

	var person Person

	db.First(&person, params["id"])
	db.Delete(&person)

	json.NewEncoder(w).Encode(&person)
}

func deleteBook(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)

	var book Book

	db.First(&book, params["id"])
	db.Delete(&book)

	json.NewEncoder(w).Encode(&book)
}